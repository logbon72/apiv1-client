<?php

namespace intelworx\tripican\apiclient;

require_once 'TripicanClient.php';
require_once 'ApiClientException.php';

/**
 * Description of API Client
 *
 * @author Joseph Orillogbon
 */
class TripicanV1Client extends TripicanClient {

    public $response = array();
    protected $debug_mode = false;
    protected $logs = array();
    protected $errors = array();
    protected static $POST_PATHS = array(
        'movies/ticket-order',
        'movies/ticket-reserve',
        'movies/ticket-cancel',
        'staff/checkin',
        'staff/checkin-reversal',
        'payment/notify',
    );

    public function __construct($format = self::FORMAT_JSON, $cache = true, $useSSL = null) {
        parent::__construct();
        $this->config['debug'] = true;
        $this->config['use_ssl'] = is_null($useSSL) ? TP_API_USE_SSL : $useSSL;
        $this->config['curl_followlocation'] = true;
        $this->verifySSL(false);
        $this->setFormat($format);
        $this->cacheRequest($cache);
    }

    public function __call($name, $arguments) {
        $servicePlusAction = TripicanApiUtils::camelCaseToDelimited($name, '-');
        $this->errors = array();
        $parts = explode('-', $servicePlusAction);
        $service = array_shift($parts);
        $this->logs[] = "Service Name:";
        $this->logs[] = $service;
        $resource = join('-', $parts);
        $this->logs[] = "Resource Name:";
        $this->logs[] = $resource;
        $path = $service . '/' . $resource;
        $this->logs[] = "Path: $path";
        if (in_array($path, self::$POST_PATHS)) {
            //use post
            $this->logs[] = "POST Request";
            return $this->post($path, empty($arguments) ? [] : (array) $arguments[0]);
        } else {
            $this->logs[] = "GET Request";
            return $this->get($path, empty($arguments) ? [] : (array) $arguments[0]);
        }
    }

    public function request($method, $url, $params = array(), $useauth = true, $multipart = false) {
        $response = parent::request($method, $url, $params, $useauth, $multipart);
        $this->logs[] = "Response code: {$response}";

        if (intval($response) === self::HTTP_OK) {
            return $this->decodedResponse();
        } else {
            $this->errors = $this->decodedResponse();
            throw new ApiClientException((array) $this->errors['errors']);
        }
    }

    private function decodedResponse() {
        if ($this->format === self::FORMAT_JSON) {
            return json_decode($this->response['response'], true);
        } else {
            return simplexml_load_string($this->response['response']);
        }
    }

    public function debug($option = false) {
        $this->debug_mode = $option;
        return $this;
    }

    public function __destruct() {
        if ($this->debug_mode) {
            echo "<pre style='border: solid 1px #ccc; background: #eee; width: 80%; margin: 10px auto; height: 400px; overflow: scroll;'>";
            echo join(PHP_EOL, $this->logs);
            echo PHP_EOL;
            print_r($this->response);
            echo "</pre>";
        }
    }

}
