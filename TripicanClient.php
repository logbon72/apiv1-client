<?php

namespace intelworx\tripican\apiclient;

use tmhOAuth;

require_once 'TripicanApiUtils.php';
require_once 'toauth.php';

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
$config = isset($GLOBALS['config']) ? $GLOBALS['config'] : [];
define('TP_API_USE_SSL', array_key_exists('use_ssl', $config) ? $config['use_ssl'] : false);
define('TP_API_CONSUMER_KEY', isset($config['api_consumer_key']) ? $config['api_consumer_key'] : '');
define('TP_API_CONSUMER_SECRET', isset($config['api_consumer_secret']) ? $config['api_consumer_secret'] : '');
define('TP_API_HOST', isset($config['api_host']) ? $config['api_host'] : 'tripican.com');
define('TP_API_END_POINT', isset($config['api_end_point']) ? $config['api_end_point'] : 'api/');

class TripicanClient extends tmhOAuth
{

    const REQUEST_METHOD_POST = 'POST';
    const REQUEST_METHOD_GET = 'GET';
    const HTTP_OK = 200;
    const API_VERSION = '1.0';
    const FORMAT_XML = 'xml';
    const FORMAT_JSON = 'json';

    protected $format = self::FORMAT_JSON;
    protected $cache_request = false;
    protected $cache_object_ttl = ResponseCache::DEFAULT_TTL;
    protected static $api_end_point = TP_API_END_POINT;

    public function __construct($consumerKey = null, $consumerSecret = null, $userToken = null, $userSecret = null, array $opts = array())
    {

        $options = array_merge($opts, array(
                'consumer_key' => $consumerKey ? $consumerKey : TP_API_CONSUMER_KEY,
                'consumer_secret' => $consumerSecret ? $consumerSecret : TP_API_CONSUMER_SECRET,
                'user_token' => $userToken,
                'user_secret' => $userSecret,
                'host' => TP_API_HOST,
                'force_nonce' => false,
                'nonce' => false, // used for checking signatures. leave as false for auto
                'force_timestamp' => false,
                'curl_timeout' => 60,
            )
        );

        parent::__construct($options);
    }

    public function setFormat($format = self::FORMAT_JSON)
    {
        $this->format = $format;
        return $this;
    }

    public function set_user_agent()
    {
        if (!empty($this->config['user_agent'])) {
            return;
        }

        if ($this->config['curl_ssl_verifyhost'] && $this->config['curl_ssl_verifypeer']) {
            $ssl = '+SSL';
        } else {
            $ssl = '-SSL';
        }

        $ua = 'TripicanAPIclient/' . self::API_VERSION . $ssl . ' - ' . $_SERVER['HTTP_HOST'];
        $this->config['user_agent'] = $ua;
    }

    public function verifySSL($verify = true)
    {
        $this->config['curl_ssl_verifyhost'] = $this->config['curl_ssl_verifypeer'] = $verify;
        return $this;
    }

    public static function setEndPoint($ep)
    {
        self::$api_end_point = $ep;
    }

    public static function getEndPoint()
    {
        return self::$api_end_point;
    }

    public function request($method, $url, $params = array(), $useauth = true, $multipart = false)
    {
        $url = $this->checkUrl($url);
        $is_cachable = $this->isCaching() && ResponseCache::isCachable($method, $url);
        ResponseCache::clearCurrentKey();


        if ($is_cachable && $method == self::REQUEST_METHOD_GET) {
            if (($cached = ResponseCache::getCached($url, $params))) {
                $this->response = $cached;
                return self::HTTP_OK;
            }
        }

        $code = parent::request($method, $url, $params, $useauth, $multipart);
        if ($code == self::HTTP_OK && $is_cachable) {
            ResponseCache::cache($url, $params, $this->response, $this->cache_object_ttl);
        }
        return $code;
    }

    public function post($url, $params = array(), $useauth = true, $multipart = false)
    {
        return $this->request(self::REQUEST_METHOD_POST, $url, $params, $useauth, $multipart);
    }

    public function get($url, $params = array(), $useauth = true, $multipart = false)
    {
        return $this->request(self::REQUEST_METHOD_GET, $url, $params, $useauth, $multipart);
    }

    protected function checkPath($path)
    {
        $pattern = '@^' . self::$api_end_point . '@';
        return preg_match($pattern, $path) ? $path : self::$api_end_point . trim($path, '/');
    }

    protected function checkUrl($url)
    {
        $pattern = '@https?://@';
        return preg_match($pattern, $url) ? $url : $this->url($url, $this->format);
    }

    public function url($request, $format = self::FORMAT_JSON)
    {
        $request = $this->checkPath($request);
        return parent::url($request, $format);
    }

    public function cacheRequest($opt = true)
    {
        $this->cache_request = $opt;
        return $this;
    }

    public function isCaching()
    {
        return $this->cache_request;
    }

    public function setCacheObjectTTL($ttl)
    {
        $this->cache_object_ttl = $ttl;
    }

    public function getCacheObjectTTL()
    {
        return $this->cache_object_ttl;
    }

}

class ResponseCache
{

    const DEFAULT_CACHE_DIR = 'cache';
    const DEFAULT_TTL = 3600;

    public static $cache_directory;
    public static $NO_CACHING_METHODS = array(
        TripicanClient::REQUEST_METHOD_POST
    );
    public static $NO_CACHING_PATTERNS = array();
    public static $currentKey = '';

    public static function setCacheDir($cacheDir = null)
    {
        if (!self::$cache_directory) {
            if (is_null($cacheDir) || !is_readable($cacheDir)) {
                self::$cache_directory = dirname(__FILE__) . DIRECTORY_SEPARATOR . self::DEFAULT_CACHE_DIR;
                if (!is_readable(self::$cache_directory)) {
                    mkdir(self::$cache_directory);
                }
            } else {
                self::$cache_directory = $cacheDir;
            }
        }
    }

    public static function cache($request, array $params, $response, $ttl = self::DEFAULT_TTL)
    {
        $key = self::getCacheKey($request, $params);
        $expiry = time() + $ttl;
        return file_put_contents($key, join(PHP_EOL, array($expiry, base64_encode(serialize($response)))));
    }

    public static function getCached($request, $params)
    {
        $key = self::getCacheKey($request, $params);
        if (file_exists($key)) {
            $object = file($key);
            if ($object[0] > time()) {
                //not expired
                return unserialize(base64_decode($object[1]));
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

    public static function clearCurrentKey()
    {
        self::$currentKey = '';
    }

    public static function getCacheKey($request, array $params)
    {
        if (!self::$currentKey) {
            self::setCacheDir();
            $prefix = str_replace('/', '.', strstr($request, TripicanClient::getEndPoint()));
            ksort($params);
            self::$currentKey = self::$cache_directory
                . DIRECTORY_SEPARATOR
                . $prefix . '_' . sha1(serialize($params)) . '.rest';
        }

        return self::$currentKey;
    }

    public static function isCachable($method, $request)
    {
        if (in_array($method, self::$NO_CACHING_METHODS)) {
            return FALSE;
        }

        if (!empty(self::$NO_CACHING_PATTERNS)) {
            $pattern = '@' . join('|', self::$NO_CACHING_PATTERNS) . '@';
            return !preg_match($pattern, $request);
        } else {
            return true;
        }
    }

}
