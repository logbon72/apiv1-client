<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace intelworx\tripican\apiclient;

/**
 * Description of ApiClientException
 *
 * @author JosephT
 */
class ApiClientException extends \Exception {

    //put your code here

    protected $errors;

    public function __construct($errors = []) {
        $this->errors = $errors;
		$joinable = [];
		foreach($errors as $err){
			$joinable[] = "[{$err['code']}] {$err['message']}";
		}
        parent::__construct(join("\n->", $joinable), null, null);
    }
    
    public function getErrors(){
        return $this->errors;
    }

}
