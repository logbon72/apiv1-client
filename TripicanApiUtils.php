<?php

namespace intelworx\tripican\apiclient;

/**
 * Description of class
 *
 * @author intelworx
 */
class TripicanApiUtils {

    const CASE_LOWER = -1;
    const CASE_NORMAL = 0;
    const CASE_UPPER = 1;
    const CAMEL_CASE_STYLE_UCFIRST = 1;
    const CAMEL_CASE_STYLE_LCFIRST = 2;

    protected static $allowedTags = "<a><b><blockquote><code><del><dd><dl><dt><em><h1><h2><h3><i><img><kbd><li><ol><p><u><pre><s><sup><sub><strike><span><font><strong><ul><br/><hr/>";

    /**
     *
     * @param string $str
     * @param string $with
     * @return string the new string 
     */
    public static function replaceNonAlphaNumeric($str, $with = '_') {
        return preg_replace('/[^A-Za-z0-9_]/', $with, $str);
    }

    /**
     * convert camelCasedString to delimeted_string
     * @param string $str camel cased string
     * @param type $delimeter
     * @param int $case to use for pieces (class::CASE_LOWER, class::CASE_UPPER, class::CASE_NORMAL), 
     * defaults to class::CASE_LOWER
     * @return string delimeted_string
     * 
     */
    public static function camelCaseToDelimited($str, $delimeter = '_', $case = self::CASE_LOWER) {
        $pieces = preg_split('/([[:upper:]][[:lower:]]+)/', $str, null, PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY);
        foreach ($pieces as $i => $piece) {
            if ($case == self::CASE_LOWER) {
                $pieces[$i] = strtolower($piece);
            } elseif ($case == self::CASE_UPPER) {
                $pieces[$i] = strtoupper($piece);
            }
        }
        return join($delimeter, $pieces);
    }

    /**
     * convert delimeted_string to camelCasedString
     * @param string $string the delimeted string to convert
     * @param string $delimeterPattern delimeter pattter to use to split string, must be compatible with preg_spit
     * defaults to _
     * @param string $style Style to use class::CAMEL_CASE_STYLE_LCFIRST (e.g. functionName, varName) or 
     * class::CAMEL_CASE_STYLE_UCFIRST (e.g. ClassName)
     * defaults to class::CAMEL_CASE_STYLE_LCFIRST
     * @return string camelCasedString
     */
    public static function delimetedToCamelCased($string, $delimeterPattern = '/_/', $style = self::CAMEL_CASE_STYLE_LCFIRST) {
        $parts = preg_split($delimeterPattern, $string);

        foreach ($parts as $i => $part) {
            if ($i == 0 && $style == self::CAMEL_CASE_STYLE_LCFIRST) {
                $parts[$i] = strtolower($part);
            } else {
                $parts[$i] = ucfirst(strtolower($part));
            }
        }

        return join('', $parts);
    }

    /**
     * remove prefix from string
     * @param str $string
     * @param str $prefix
     * @return string
     */
    public static function removePrefixFromString($string, $prefix) {
        return substr($string, strlen($prefix));
    }

    public static function getAllowedTags() {
        return self::$allowedTags;
    }

    public static function stripTags($str) {
        return strip_tags($str, self::$allowedTags);
    }

    public static function sanitize($str, $separator = '-', $style = self::CASE_LOWER) {
        $sanitised = preg_replace('/[^a-zA-Z0-9-_]+/', $separator, $str);
        if ($style == self::CASE_UPPER) {
            return strtoupper($sanitised);
        } else if ($style == self::CASE_LOWER) {
            return strtolower($sanitised);
        } else {
            return $sanitised;
        }
    }

}
