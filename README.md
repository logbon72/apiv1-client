#Overview#

This is a PHP client implementation for tripican.com API (V1).

#How to Get API Key#

Please send a message to bizdev[at]tripican[dot]com describing your use case, you message will be reviewed and you will be notified.

#Client Usage#

```
#!php

require_once '/path/to/TripicanV1Client.php';

use intelworx\tripican\apiclient\TripicanV1Client;

$config = array(
    'api_consumer_key' => '---SOME-CONSUMER-KEY---',
    'api_consumer_secret' => '----SOME-SECRET-----',
    'api_host' => 'tripican.com', //or beta20.tripican.com
);


$apiClient = new TripicanV1Client();

//if you wish to debug responses.
$apiClient->debug(true);

//sample call
//short hand method transforms serviceResourceName to service/resource-name
//it knows resources that are expected to be called with POST,
//however to be safe, you may want to use the full call method for post
//e.g. $apiClient->post($path, $params);
$result = $apiClient->moviesShowtimes();
var_dump($result);

```

